import traceback
import logging
import pymysql
import json
import uuid
import sys
import os

logger = logging.getLogger()
logger.setLevel(logging.INFO)

def openConnection():
    """Generic MySQL DB connection handler"""
    global Connection
    try:
        Connection = pymysql.connect(os.environ['v_db_host'], os.environ['v_username'], os.environ['v_password'], os.environ['v_database'], connect_timeout=5)
    except Exception as e:
        logger.error(e)
        logger.error("ERROR: Unexpected error: Could not connect to MySql instance.")
        raise e


def fn_checkBrandExist(lv_CutitronicsBrandID):

    lv_list = ""
    lv_statement = "SELECT CutitronicsBrandID, BrandName, ContactName, ContactTitle, ContactEmail FROM BrandDetails WHERE CutitronicsBrandID = %(CutitronicsBrandID)s"
    try:
        with Connection.cursor() as cursor:
            cursor.execute(lv_statement, {'CutitronicsBrandID': lv_CutitronicsBrandID})
            
            for row in cursor:
                field_names = [i[0] for i in cursor.description]
                lv_list = dict(zip(field_names, row))

            if len(lv_list) == 0:
                return(400)
            else:
                return lv_list

    except pymysql.err.IntegrityError as e:
        logging.info(e.args)
        return 500


def fn_createQuestion(lv_CutitronicsBrandID, lv_QuestionKey, lv_QuestionSet, lv_QuestionText, lv_CreationUser):


    lv_statement = "INSERT INTO ClientSurveyQuestion (QuestionKey, QuestionSet, CutitronicsBrandID, EnabledFlag, QuestionText, CreationDate, Creationuser) VALUES (%(QuestionKey)s, %(QuestionSet)s, %(CutitronicsBrandID)s, 'True', %(QuestionText)s, SYSDATE(), %(CreationUser)s)"

    try:
        with Connection.cursor() as cursor:
            cursor.execute(lv_statement, {'QuestionKey': lv_QuestionKey, 'QuestionSet': lv_QuestionSet, 'CutitronicsBrandID': lv_CutitronicsBrandID, 'QuestionText': lv_QuestionText, 'CreationUser': lv_CreationUser})

            if cursor.rowcount == 1:
                return(lv_QuestionKey)
            else:
                return 400
    except pymysql.err.InternalError as e:
        logger.error(e.args)
        return(500)
    except pymysql.err.DataError as e:
        logger.error(e.args)
        return(500)
    except pymysql.err.IntegrityError as e:
        logger.error(e.args)
        return(500)


def lambda_handler(event, context):
    """
    createQuestion

    This is used to register a User at the Back Office.
    """

    openConnection()

    logger.info(" ")
    logger.info(
        "Lambda function - {function_name}".format(function_name=context.function_name))
    logger.info(" ")
    logger.info(" ")
    logger.info("Event - %s", event)
    logger.info(" ")

    # Context Dump
    logger.info(" ")
    logger.info("Context.function_name - %s", context.function_name)
    logger.info(" ")

    #Variables
    lv_CutitronicsBrandID = None
    lv_QuestionSet = None
    lv_QuestionText = None

    try:
        try:
            body = json.loads(event['body'])
        except KeyError as e:
            try:
                body = event
            except TypeError:
                app_json = json.dumps(event)
                event = {"body": app_json}
                body = json.loads(event['body'])

        logger.info("Payload body - '{body}'".format(body=body))

        lv_CutitronicsBrandID = body.get('CutitronicsBrandID')
        lv_QuestionSet = body.get('QuestionSet')
        lv_QuestionText = body.get('QuestionText')

        createQuestion_out = {"Service": context.function_name, "Status": "Success", "ErrorCode": "", "ErrorDesc": "", "ErrorStack": "", "ServiceOutput": {}}

        """
        1. Check Brand Exists
        """
        lv_BrandType = fn_checkBrandExist(lv_CutitronicsBrandID)

        if lv_BrandType == 400:

            logger.error("")
            logger.error("Could not find brand '{BrandID}' in the back office systems".format(BrandID=lv_CutitronicsBrandID))
            logger.error("")

            createQuestion_out['Status'] = "Error"
            createQuestion_out['ErrorCode'] = context.function_name + "_001"  # viewGuests_out_001
            createQuestion_out['ErrorDesc'] = "Supplied CutitronicsBrandID does not exist in the BO Database"
            createQuestion_out['ServiceOutput']['CutitronicsBrandID'] = body.get('CutitronicsBrandID')
            createQuestion_out['ServiceOutput']['QuestionSet'] = body.get('QuestionSet')
            createQuestion_out['ServiceOutput']['QuestionText'] = body.get('QuestionText')

            Connection.close()

            # Lambda response

            return {
                "isBase64Encoded": "false",
                "statusCode": 400,
                "headers": {"x-custom-header": context.function_name, 'Access-Control-Allow-Origin': '*'},
                "body": json.dumps(createQuestion_out)
            }
        
        logger.info("")
        logger.info("Found brand '{BrandID}' in the back office".format(BrandID=lv_CutitronicsBrandID))
        logger.info("")

        """
        2. Create new question key
        """

        lv_QuestionKey = str(uuid.uuid4())

        """
        3. Insert new question to back office
        """

        lv_QuestionType = fn_createQuestion(lv_CutitronicsBrandID, lv_QuestionKey, lv_QuestionSet, lv_QuestionText, context.function_name)

        if lv_QuestionType == 400:

            logger.error("")
            logger.error("Could not insert question in the back office systems")
            logger.error("")

            createQuestion_out['Status'] = "Error"
            createQuestion_out['ErrorCode'] = context.function_name + "_002"  # viewGuests_out_001
            createQuestion_out['ErrorDesc'] = "Could not insert question in the back office systems"
            createQuestion_out['ServiceOutput']['CutitronicsBrandID'] = body.get('CutitronicsBrandID')
            createQuestion_out['ServiceOutput']['QuestionSet'] = body.get('QuestionSet')
            createQuestion_out['ServiceOutput']['QuestionText'] = body.get('QuestionText')

            Connection.rollback()
            Connection.close()

            # Lambda response

            return {
                "isBase64Encoded": "false",
                "statusCode": 400,
                "headers": {"x-custom-header": context.function_name, 'Access-Control-Allow-Origin': '*'},
                "body": json.dumps(createQuestion_out)
            }

        """
        4. Build output
        """

        Connection.commit()
        Connection.close()

        createQuestion_out['Status'] = "Success"
        createQuestion_out['ErrorCode'] = context.function_name + "_000"  # viewGuests_out_001
        createQuestion_out['ErrorDesc'] = ""
        createQuestion_out['ServiceOutput']['CutitronicsBrandID'] = body.get('CutitronicsBrandID')
        createQuestion_out['ServiceOutput']['QuestionSet'] = body.get('QuestionSet')
        createQuestion_out['ServiceOutput']['QuestionText'] = body.get('QuestionText')
        createQuestion_out['ServiceOutput']['QuestionKey'] = lv_QuestionKey

        logger.info("")
        logger.info("Successful execution, return to caller '{Out}'".format(Out=json.dumps(createQuestion_out)))
        logger.info("")

        # Lambda respose
        return {
            "isBase64Encoded": "false",
            "statusCode": 200,
            "headers": {"x-custom-header": context.function_name, 'Access-Control-Allow-Origin': '*'},
            "body": json.dumps(createQuestion_out)
        }

    except Exception as e:
        logger.error("CATCH-ALL ERROR: Unexpected error: '{error}'".format(error=e))

        exception_type, exception_value, exception_traceback = sys.exc_info()
        traceback_string = traceback.format_exception(exception_type, exception_value, exception_traceback)
        err_msg = json.dumps({
            "errorType": exception_type.__name__,
            "errorMessage": str(exception_value),
            "stackTrace": traceback_string
        })
        logger.error(err_msg)

        Connection.rollback()
        Connection.close()

        # Populate Cutitronics reply block

        createQuestion_out['Status'] = "Error"
        createQuestion_out['ErrorCode'] = context.function_name + "_000"  # CreateTherapistSession_000
        createQuestion_out['ErrorDesc'] = "CatchALL"
        createQuestion_out["ServiceOutput"]["CutitronicsBrandID"] = body.get('CutitronicsBrandID', "")

        # Lambda response

        return {
            "isBase64Encoded": "false",
            "statusCode": 500,
            "headers": {"x-custom-header": context.function_name, 'Access-Control-Allow-Origin': '*'},
            "body": json.dumps(createQuestion_out)
        }

# class context:
#     def __init__(self):
#         self.function_name = "createQuestion"

# context = context()
# event = {
#     "CutitronicsBrandID": "0002",
#     "QuestionSet": "TestQuestions",
#     "QuestionText": "TestQ2"
# }

# x = lambda_handler(event, context)

# print(x)